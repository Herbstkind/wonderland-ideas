#!/usr/bin/bash

function error {
    (>&2 echo "Error: $1")
    exit 1
}

# make some statements
scriptdir=$(dirname "$0")
hook=$(head -1 ~/.wonderland-ideas-hook-text)
if [ -z "$hook" ]; then
    error "Can't read hook from hook file"
fi
usedtexts=~/.wonderland-ideas-used-texts
touch "$usedtexts"
message=''

# reuse if all texts have been used up
if [ "$(wc -l "$scriptdir/texts"|cut -d' ' -f1)" -eq "$(wc -l "$usedtexts"|cut -d' ' -f1)" ]; then
    message=$(echo -e "All texts have been used up. I'm reusing now.\n\n ")
    echo -n "" > "$usedtexts"
fi

# think of something
cont="yes"
while [ -n "$cont" ]; do
    idea=$(shuf -n 1 "$scriptdir"/texts)
    if ! grep -F "$idea" "$usedtexts" > /dev/null; then cont=''; fi
done
echo "$idea" >> "$usedtexts"
message="$message$idea"

# warn if we're one text away from reusing
if [ "$(wc -l "$scriptdir/texts"|cut -d' ' -f1)" -eq "$(wc -l "$usedtexts"|cut -d' ' -f1)" ]; then
    message=$(echo -e "$message\n\nWarning: All texts have been used up. Please add new texts or I'll be reusing next time.")
fi


# send message
message=$(echo -e "$message\n\n*(If you have any wonderland ideas, send them to Felia!)*")
if [ -z "$1" ]; then  # dry run when arg supplied
    response=$(curl -F "content=$message" "$hook")
else
    echo "$message"
fi

# Check server response
if [ -n "$response" ]; then
    error "Could not post idea"
fi
