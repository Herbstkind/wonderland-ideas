#!/usr/bin/bash

keyword='landscape'

domain="www.wallpapermaiden.com"
site_root="https://$domain/tag/$keyword"

# find out maximum page number
highest_page=0
for num in $(curl "$site_root?page=1"|grep -o '?page=[[:digit:]]\+'|grep -o '[[:digit:]]\+'); do
    if [ "$num" -gt "$highest_page" ]; then
        highest_page="$num"
    fi
done

# select random page
page=$((RANDOM % highest_page + 1))

# get image site
site_overview="$site_root?page=$page"
site_image=$(curl "$site_overview"|grep "<a href=\"https://$domain/wallpaper/"|grep -o 'https[^"]\+'|shuf -n1)

# get image URL
url_resolution_original=$(curl "$site_image"|paste -s|sed 's/^.*<span\s\+class="[^"]\+TitleText">ORIGINAL<\/span>//'|grep -o "https://$domain/wallpaper/[^\"]\+"|head -1)
url_image=$(curl "$url_resolution_original"|grep -o "<img src=\"https://$domain/image/[^\"]\+"|grep -o 'https[^"]\+'|head -1)

# return
echo -n "$url_image"
