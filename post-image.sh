#!/usr/bin/bash

function error {
	(>&2 echo "Error: $1")
	exit 1
}

# make some statements
scriptdir=$(dirname "$0")
hook=$(head -1 ~/.wonderland-ideas-hook-image)
if [ -z "$hook" ]; then
    error "Can't read hook from hook file"
fi

# getter script
getter="image-getters/wallpapermaiden.sh"

# get image URL
url=$("$scriptdir/$getter")
if [ -z "$1" ]; then  # dry run when arg supplied
    response=$(curl -F "content=$url" "$hook")
else
    echo "$url"
fi
